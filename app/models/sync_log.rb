class SyncLog < ActiveRecord::Base
  acts_as_paranoid :column => 'deleted_at', :column_type => 'time'
end
