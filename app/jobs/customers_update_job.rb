class CustomersUpdateJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    @shop = Shop.find_by(shopify_domain: args[1])
    if !@shop.nil?
      init_lock_manager
      job_lock = @lock_manager.lock(args[0]["id"], ENV["WEBHOOK_JOB_LOCK_DELAY"].to_i)
      if job_lock
        @shop.create_customer(args[0], "put")
        @lock_manager.unlock(job_lock)
      end
    end
  end

  private
    def init_lock_manager
      @lock_manager = Redlock.new("redis://#{ENV["REDIS_SERVER"]}:#{ENV["REDIS_PORT"]}")
    end
end
