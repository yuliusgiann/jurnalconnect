class ShopSyncJob < ActiveJob::Base
  include DatabaseModule
  queue_as :default

  def perform(shop)
    shop.sync_all_data

  rescue Exception => e
    puts e.message
    puts e.backtrace.inspect

    update_sync_status(shop, false)
  end
end
