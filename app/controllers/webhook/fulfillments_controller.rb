class Webhook::FulfillmentsController < ApplicationController
  include ConnectionModule
  include DatabaseModule
  include ShopifyApp::WebhookVerification

  before_filter :prepare_notifier

  def create
    OrdersUpdatedJob.perform_later(fulfillments_params, request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"])
    head :ok
  end

  private

  def prepare_notifier
    shop = request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"]
    if shop != nil
      request.env["exception_notifier.exception_data"] = {
        :current_shop => shop  
      }
    end
  end

  def find_shop
    Shop.find_by(shopify_domain: "#{request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"]}")
  end

  def fulfillments_params
    shop = find_shop
    order = ShopifyAPI::Session.temp("#{shop.shopify_domain}", "#{shop.shopify_token}") {ShopifyAPI::Order.find(params["order_id"])}
    JSON.parse(order.to_json)
  end
end