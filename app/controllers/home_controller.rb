class HomeController < ShopifyApp::AuthenticatedController
  # Root url, check if the user already has an access token
  # If no access token, redirect the user to login page
  # Redirection is done in the view component
  def index
    @shop = Shop.find_by(shopify_token: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).token}")
    unless @shop.access_token.nil?
      @authorized = true
    end
  end

  # Receive callback from Jurnal oauth and store the access token
  def callback
    @shop = Shop.find_by(shopify_token: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).token}")
    if params["access_token"]
      @shop.access_token = params["access_token"]
      @shop.save
    end
    redirect_to root_path
  end
end