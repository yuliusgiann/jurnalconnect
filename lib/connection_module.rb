module ConnectionModule
  require 'net/https'

  def request_from_jurnal(url, shop_domain)
    shop = Shop.find_by(shopify_domain: shop_domain)
    if shop != nil && shop.access_token != nil
      url = "#{url}?access_token=#{shop.access_token}"
      uri = URI.parse(url)
      puts "Sending data to #{uri.to_s}"
      req = Net::HTTP::Get.new(uri.to_s, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
      response = Net::HTTP.start(uri.host, 443, use_ssl: true) {|http| http.request(req)}
      # response = Net::HTTP.start(uri.host, uri.port) {|http| http.request(req)}
    end
  end

  def sendJSON(targetURL, payload, shop_domain, type)
    shop = Shop.find_by(shopify_domain: shop_domain)
    if shop != nil && shop.access_token != nil
      targetURL = "#{targetURL}?access_token=#{shop.access_token}"
      uri = URI.parse(targetURL)
      puts "Sending data to #{uri.to_s}"
      req = http_type(type).new(uri.to_s, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
      req.body = payload
      response = Net::HTTP.start(uri.host, 443, use_ssl: true){|http| http.request(req)}
      # response = Net::HTTP.start(uri.host, uri.port) {|http| http.request(req)}
    end     
  end

  private
    def http_type(type)
      "Net::HTTP::#{type.camelize}".safe_constantize
    end
end