ShopifyApp.configure do |config|
  config.api_key = ENV["API_KEY"]
  config.secret = ENV["SHARED_SECRET"]
  config.scope = "read_orders, read_products, read_customers, read_fulfillments, read_shipping"
  config.embedded_app = true
  config.webhooks = [
    {topic: 'app/uninstalled', address: ENV["UNINSTALL_PATH"]},
  # #   {topic: 'customers/create', address: ENV["CUSTOMER_CREATE_PATH"]},
  # #   {topic: 'customers/update', address: ENV["CUSTOMER_UPDATE_PATH"]},
  # #   {topic: 'products/create', address: ENV["PRODUCT_CREATE_PATH"]},
  # #   {topic: 'orders/create', address: ENV["ORDER_CREATE_PATH"]},
  # #   {topic: 'order_transactions/create', address: ENV["TRANSACTION_CREATE_PATH"]},
  ]
end
