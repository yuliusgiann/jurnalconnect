class AddDeletedAtToShops < ActiveRecord::Migration
  def change
    add_column :shops, :deleted_at, :string
  end
end
