class AddDeletedAtToSentData < ActiveRecord::Migration
  def change
    add_column :sent_data, :deleted_at, :string
  end
end
