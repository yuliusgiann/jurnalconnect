class RemoveApiKeyFromLogs < ActiveRecord::Migration
  def change
  	remove_column :logs, :api_key, :string
  end
end
