class ChangeApiKeyColumnName < ActiveRecord::Migration
  def change
  	rename_column :shops, :api_key, :access_token
  end
end
