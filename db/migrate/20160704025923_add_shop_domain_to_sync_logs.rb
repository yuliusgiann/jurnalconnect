class AddShopDomainToSyncLogs < ActiveRecord::Migration
  def change
  	add_column :sync_logs, :shop_domain, :string, null: false
  end
end
