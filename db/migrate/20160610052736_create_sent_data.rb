class CreateSentData < ActiveRecord::Migration
  def change
    create_table :sent_data do |t|
    	t.string :kind
    	t.string :created_at_shopify
    	t.timestamps null: false
    end
  end
end
