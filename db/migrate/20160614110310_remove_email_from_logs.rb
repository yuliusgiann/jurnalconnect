class RemoveEmailFromLogs < ActiveRecord::Migration
  def change
    remove_column :logs, :email, :string
  end
end
